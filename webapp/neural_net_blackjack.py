import numpy as np
import pandas as pd
import random
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, LSTM, Flatten, Dropout

# This function lists out all permutations of ace values in the array sum_array
# For example, if you have 2 aces, there are 4 permutations:
#     [[1,1], [1,11], [11,1], [11,11]]
# These permutations lead to 3 unique sums: [2, 12, 22]
# Of these 3, only 2 are <=21 so they are returned: [2, 12]
def get_ace_values(temp_list):
    sum_array = np.zeros((2**len(temp_list), len(temp_list)))
    # This loop gets the permutations
    for i in range(len(temp_list)):
        n = len(temp_list) - i
        half_len = int(2**n * 0.5)
        for rep in range(int(sum_array.shape[0]/half_len/2)):
            sum_array[rep*2**n : rep*2**n+half_len, i] = 1
            sum_array[rep*2**n+half_len : rep*2**n+half_len*2, i] = 11
    # Only return values that are valid (<=21)
    # return list(set([int(s) for s in np.sum(sum_array, axis=1) if s<=21]))
    return [int(s) for s in np.sum(sum_array, axis=1)]

# Convert num_aces, an int to a list of lists
# For example if num_aces=2, the output should be [[1,11],[1,11]]
# I require this format for the get_ace_values function
def ace_values(num_aces):
    temp_list = []
    for i in range(num_aces):
        temp_list.append([1, 11])
    return get_ace_values(temp_list)


# Make a deck
def make_decks(num_decks, card_types):
    new_deck = []
    for i in range(num_decks):
        for j in range(4):
            new_deck.extend(card_types)
    random.shuffle(new_deck)
    return new_deck


# Total up value of hand
def total_up(hand):
    aces = 0
    total = 0

    for card in hand:
        if card != 'A':
            total += card
        else:
            aces += 1

    # Call function ace_values to produce list of possible values for aces in hand
    ace_value_list = ace_values(aces)

    final_totals = [i + total for i in ace_value_list if i + total <= 21]

    if final_totals == []:
        return min(ace_value_list) + total
    else:
        return max(final_totals)


# Play a game of blackjack (after the cards are dealt)
def play_game(dealer_hand, player_hands, blackjack, curr_player_results, dealer_cards, hit_stay, live_total):
    action = 0
    # Dealer checks for 21
    if set(dealer_hand) == blackjack:
        for player in range(players):
            if set(player_hands[player]) != blackjack:
                curr_player_results[0, player] = -1
            else:
                curr_player_results[0, player] = 0
    else:
        for player in range(players):
            # Players check for 21
            if set(player_hands[player]) == blackjack:
                curr_player_results[0, player] = 1
            else:
                # Hit randomly, check for busts
                if (hit_stay >= 0.5) and (total_up(player_hands[player]) != 21):
                    player_hands[player].append(dealer_cards.pop(0))
                    action = 1
                    live_total.append(total_up(player_hands[player]))
                    if total_up(player_hands[player]) > 21:
                        curr_player_results[0, player] = -1

    # Dealer hits based on the rules
    while total_up(dealer_hand) < 17:
        dealer_hand.append(dealer_cards.pop(0))
    # Compare dealer hand to players hand but first check if dealer busted
    if total_up(dealer_hand) > 21:
        for player in range(players):
            if curr_player_results[0, player] != -1:
                curr_player_results[0, player] = 1
    else:
        for player in range(players):
            if total_up(player_hands[player]) > total_up(dealer_hand):
                if total_up(player_hands[player]) <= 21:
                    curr_player_results[0, player] = 1
            elif total_up(player_hands[player]) == total_up(dealer_hand):
                curr_player_results[0, player] = 0
            else:
                curr_player_results[0, player] = -1

    return curr_player_results, dealer_cards, action


players = 1
num_decks = 1

card_types = ['A', 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]

dealer_card_feature = []
player_card_feature = []
player_live_total = []
player_live_action = []
player_results = []

model_df = pd.DataFrame()

def start():
    stacks = 50000
    for stack in range(stacks):
        blackjack = set(['A', 10])
        dealer_cards = make_decks(num_decks, card_types)
        while len(dealer_cards) > 20:

            curr_player_results = np.zeros((1, players))

            dealer_hand = []
            player_hands = [[] for player in range(players)]
            live_total = []
            live_action = []

            # Deal FIRST card
            for player, hand in enumerate(player_hands):
                player_hands[player].append(dealer_cards.pop(0))
            dealer_hand.append(dealer_cards.pop(0))
            # Deal SECOND card
            for player, hand in enumerate(player_hands):
                player_hands[player].append(dealer_cards.pop(0))
            dealer_hand.append(dealer_cards.pop(0))

            # Record the player's live total after cards are dealt
            live_total.append(total_up(player_hands[player]))

            if stack < 25000:
                hit_stay = 1
            else:
                hit_stay = 0
            curr_player_results, dealer_cards, action = play_game(dealer_hand, player_hands,
                                                                  blackjack, curr_player_results,
                                                                  dealer_cards, hit_stay, live_total)

            # Track features
            dealer_card_feature.append(dealer_hand[0])
            player_card_feature.append(player_hands)
            player_results.append(list(curr_player_results[0]))
            player_live_total.append(live_total)
            player_live_action.append(action)

    model_df['dealer_card'] = dealer_card_feature
    model_df['player_total_initial'] = [total_up(i[0][0:2]) for i in player_card_feature]
    model_df['hit?'] = player_live_action

    has_ace = []
    for i in player_card_feature:
        if ('A' in i[0][0:2]):
            has_ace.append(1)
        else:
            has_ace.append(0)
    model_df['has_ace'] = has_ace

    dealer_card_num = []
    for i in model_df['dealer_card']:
        if i == 'A':
            dealer_card_num.append(11)
        else:
            dealer_card_num.append(i)
    model_df['dealer_card_num'] = dealer_card_num

    model_df['Y'] = [i[0] for i in player_results]
    lose = []
    for i in model_df['Y']:
        if i == -1:
            lose.append(1)
        else:
            lose.append(0)
    model_df['lose'] = lose

    correct = []
    for i, val in enumerate(model_df['lose']):
        if val == 1:
            if player_live_action[i] == 1:
                correct.append(0)
            else:
                correct.append(1)
        else:
            if player_live_action[i] == 1:
                correct.append(1)
            else:
                correct.append(0)
    model_df['correct_action'] = correct



players = 1
num_decks = 1

card_types = ['A', 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]

dealer_card_feature = []
player_card_feature = []
player_results = []

def start2():
    stacks = 50000
    for stack in range(stacks):
        blackjack = set(['A', 10])
        dealer_cards = make_decks(num_decks, card_types)
        while len(dealer_cards) > 20:

            curr_player_results = np.zeros((1, players))

            dealer_hand = []
            player_hands = [[] for player in range(players)]

            # Deal FIRST card
            for player, hand in enumerate(player_hands):
                player_hands[player].append(dealer_cards.pop(0))
            dealer_hand.append(dealer_cards.pop(0))
            # Deal SECOND card
            for player, hand in enumerate(player_hands):
                player_hands[player].append(dealer_cards.pop(0))
            dealer_hand.append(dealer_cards.pop(0))

            # Dealer checks for 21
            if set(dealer_hand) == blackjack:
                for player in range(players):
                    if set(player_hands[player]) != blackjack:
                        curr_player_results[0, player] = -1
                    else:
                        curr_player_results[0, player] = 0
            else:
                for player in range(players):
                    # Players check for 21
                    if set(player_hands[player]) == blackjack:
                        curr_player_results[0, player] = 1
                    else:
                        # Hit only when we know we will not bust
                        while total_up(player_hands[player]) <= 11:
                            player_hands[player].append(dealer_cards.pop(0))
                            if total_up(player_hands[player]) > 21:
                                curr_player_results[0, player] = -1
                                break

            # Dealer hits based on the rules
            while total_up(dealer_hand) < 17:
                dealer_hand.append(dealer_cards.pop(0))
            # Compare dealer hand to players hand but first check if dealer busted
            if total_up(dealer_hand) > 21:
                for player in range(players):
                    if curr_player_results[0, player] != -1:
                        curr_player_results[0, player] = 1
            else:
                for player in range(players):
                    if total_up(player_hands[player]) > total_up(dealer_hand):
                        if total_up(player_hands[player]) <= 21:
                            curr_player_results[0, player] = 1
                    elif total_up(player_hands[player]) == total_up(dealer_hand):
                        curr_player_results[0, player] = 0
                    else:
                        curr_player_results[0, player] = -1
            # print('player: ' + str(total_up(player_hands[player])),
            #      'dealer: ' + str(total_up(dealer_hand)),
            #      'result: ' + str(curr_player_results)
            #     )

            # Track features
            dealer_card_feature.append(dealer_hand[0])
            player_card_feature.append(player_hands)
            player_results.append(list(curr_player_results[0]))

    model_df_naive = pd.DataFrame()
    model_df_naive['dealer_card'] = dealer_card_feature
    model_df_naive['player_total_initial'] = [total_up(i[0][0:2]) for i in player_card_feature]
    model_df_naive['Y'] = [i[0] for i in player_results]

    lose = []
    for i in model_df_naive['Y']:
        if i == -1:
            lose.append(1)
        else:
            lose.append(0)
    model_df_naive['lose'] = lose

    has_ace = []
    for i in player_card_feature:
        if ('A' in i[0][0:2]):
            has_ace.append(1)
        else:
            has_ace.append(0)
    model_df_naive['has_ace'] = has_ace

    dealer_card_num = []
    for i in model_df_naive['dealer_card']:
        if i == 'A':
            dealer_card_num.append(11)
        else:
            dealer_card_num.append(i)
    model_df_naive['dealer_card_num'] = dealer_card_num

    # Treniranje mreže

    # Nastavi spremenljivke za mrežo
    feature_list = [i for i in model_df.columns if i not in ['dealer_card',
                                                             'Y', 'lose',
                                                             'correct_action']]
    train_X = np.array(model_df[feature_list])
    train_Y = np.array(model_df['correct_action']).reshape(-1, 1)

    model = Sequential()
    # model.add(Dense(16, activation='relu'))
    # model.add(Dense(128, activation='relu'))
    # model.add(Dense(32, activation='relu'))
    # model.add(Dense(8, activation='relu'))
    model.add(Dense(16))
    model.add(Dense(128))
    model.add(Dense(32))
    model.add(Dense(8))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='sgd')
    model.fit(train_X, train_Y, epochs=20, batch_size=256, verbose=0)

    pred_Y_train = model.predict(train_X)
    actuals = train_Y[:,-1]

    model.save('my_model.h5')

def read_file():
    global model
    model = tf.keras.models.load_model('my_model.h5')

def model_decision(player_sum, has_ace, dealer_card_num):
    model = tf.keras.models.load_model('my_model.h5')
    input_array = np.array([player_sum, 0, has_ace, dealer_card_num]).reshape(1, -1)
    predict_correct = model.predict(input_array)
    if predict_correct >= 0.52:
        return 1
    else:
        return 0

# start()
# start2()