from grpc import StatusCode
import flask
from flask import request, jsonify
from neural_net_blackjack import model_decision

app = flask.Flask(__name__)
app.config["DEBUG"] = True


@app.route('/', methods=['GET'])
def home():
    return "<h1>Distant Reading Archive</h1><p>This site is a prototype API for distant reading of science fiction novels.</p>"

@app.route('/hints', methods=['GET'])
def hints():
    player_hand = request.args.get('playerHand')
    has_ace = request.args.get('hasAce')
    dealer_hand = request.args.get('dealerHand')

    if not (player_hand or has_ace or dealer_hand):
        return StatusCode(404)
    
    decision = model_decision(int(player_hand), int(has_ace), int(dealer_hand))

    json = {
            'decision': decision
    }

    return jsonify(json)

app.run(host="0.0.0.0")